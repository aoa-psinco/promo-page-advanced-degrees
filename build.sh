#!/usr/bin/env bash

ITERATION=$1
DIRECTORY="togo"
STYLESHEET_NAME="styles-$ITERATION.css"
HTML_NAME="markup-$ITERATION.html"

if [[ ! -d $DIRECTORY ]] ; then
  mkdir $DIRECTORY
fi

cp ./dist/styles.css "$DIRECTORY"/"$STYLESHEET_NAME"
